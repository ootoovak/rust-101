use std::io::prelude::*;
use std::io;

fn read_vec() -> Vec<f32> {
    let mut vec: Vec<f32> = Vec::<f32>::new();
    let stdin = io::stdin();
    println!("Enter a list of numbers, one per line. End with Ctrl-D.");
    for line in stdin.lock().lines() {
        let line = line.unwrap();
        match line.trim().parse::<f32>() {
            Ok(num) => vec.push(num),
            Err(_) => println!("What did I say about numbers?"),
        }
    }
    vec
}

use part02::{SomethingOrNothing, Something, Nothing, vec_min};

pub trait Print {
    fn print2(self);
}

impl Print for f32 {
    fn print2(self) {
        println!("The number is: {}", self)
    }
}

impl<T: Print> SomethingOrNothing<T> {
    fn print2(self) {
        match self {
            Nothing => println!("The number is: <nothing>"),
            Something(n) => n.print2(),
        }
    }
}

pub fn main() {
    let vec = read_vec();
    let min = vec_min(vec);
    min.print2();
}
