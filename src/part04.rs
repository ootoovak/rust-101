// void foo(std::vector<int> v) {
// int *first = &v[0];
// v.push_back(42);
// first = 1337; // This is bad!
// }
//

fn work_on_vector(v: Vec<i32>) {
    // do something
}

fn ownership_demo() {
    let v = vec![1, 2, 3, 4];
    work_on_vector(v);
    // println!("The first element is {}" v[0]); // BAD!
}

fn vec_min(v: &Vec<i32>) -> Option<i32> {
    use std::cmp;

    let mut min = None;
    for e in v.iter() {
        min = Some(match min {
            None => *e,
            Some(n) => cmp::min(n, *e),
        });
    }
    min
}

fn shared_ref_demo() {
    let v = vec![5, 4, 3, 2, 1];
    let first = &v[0];
    vec_min(&v);
    vec_min(&v);
    println!("The first element is: {}", *first);
}

fn vec_inc(v: &mut Vec<i32>) {
    for e in v.iter_mut() {
        *e += 1;
    }
}

fn mutable_ref_demo() {
    let mut v = vec![5, 4, 3, 2, 1];
    // let first = &v[0];
    vec_inc(&mut v);
    vec_inc(&mut v);
    // println!("The first element is: {}", *first); // BAD!
}
